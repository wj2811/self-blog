package com.wj.controller;


import com.wj.pojo.Article;
import com.wj.service.IArticleService;
import com.wj.utils.FastDFSUtil;
import com.wj.utils.RespBean;
import com.wj.vo.PagingVO;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wj
 * @since 2022-05-31
 */
@RestController
@RequestMapping("/article")
public class ArticleController {
    @Resource
    private IArticleService articleService;

    /**
     * 根据id获取对应文章
     * @param id
     * @return
     */
    @GetMapping("/blog/{id}")
    public RespBean getArticleById(@PathVariable Integer id){
        return articleService.getArticleById(id);
    }

    /**
     * 后端获取文章
     * @param pagingVO
     * @return
     */
    @PostMapping("/getArticles")
    public RespBean getPagingArticle(@Valid @RequestBody PagingVO pagingVO){
        return articleService.getPagingArticle(pagingVO);
    }

    /**
     * 首页展示文章
     * @param pagingVO
     * @return
     */
    @PostMapping("/blog/getHomeArticles")
    public RespBean getHomeArticles(@Valid @RequestBody PagingVO pagingVO){
        return articleService.getHomeArticles(pagingVO);
    }

    /**
     * 修改文章
     * @param article
     * @return
     */
    @PutMapping("/")
    public RespBean upArticle(@RequestBody Article article){
        if (articleService.updateById(article)){
            return RespBean.success("修改成功");
        }
        return RespBean.error("修改失败");
    }

    /**
     * 添加文章
     * @param article
     * @return
     */
    @PostMapping("/")
    public RespBean addArticle(@Valid @RequestBody Article article){
        return articleService.addArticle(article);
    }

    /**
     * 写文章时上传图片
     * @param file
     * @return
     */
    @PostMapping("/img")
    public RespBean uploadImage(MultipartFile file){
        String upload = FastDFSUtil.upload(file);
        if (StringUtils.hasText(upload)){
            return RespBean.success("成功",upload);
        }
        return RespBean.error();
    }

}

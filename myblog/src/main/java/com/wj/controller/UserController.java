package com.wj.controller;


import com.wj.pojo.User;
import com.wj.service.IUserService;
import com.wj.utils.RespBean;
import com.wj.vo.PagingVO;
import com.wj.vo.UserVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wj
 * @since 2022-05-26
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private IUserService userService;

    @PutMapping("/")
    public RespBean upUser(@RequestBody User user){
        if (userService.updateById(user)){
            return RespBean.success("修改成功");
        }
        return RespBean.error("修改失败");
    }

    @PostMapping("/register")
    public RespBean register(@Valid @RequestBody UserVo user) {
        return userService.registerUser(user);
    }

    @PostMapping("/getUsers")
    public RespBean getPagingUsers(@Valid @RequestBody PagingVO pagingVo){
        return userService.getPagingUsers(pagingVo);
    }

}

package com.wj.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author wj
 * @since 2022-05-29
 */
@Getter
@Setter
@TableName("t_user_info")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //邮箱
    @TableField("email")
    private String email;

    //别名
    @TableField("nickname")
    private String nickname;

    //头像
    @TableField("avatar")
    private String avatar;

    //简介
    @TableField("intro")
    private String intro;

    @TableField("web_site")
    private String webSite;

    //创建时间
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private LocalDateTime createTime;

    //最后登录时间
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy:MM-dd HH:mm",timezone = "Asia/Shanghai")
    private LocalDateTime updateTime;

    //账号是否禁用
    @TableField("is_disable")
    private Boolean isDisable;


}

package com.wj.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wj.config.CommonConstant;
import com.wj.dto.PagingDTO;
import com.wj.dto.UserInfoDTO;
import com.wj.enums.LoginTypeEnum;
import com.wj.enums.RoleEnum;
import com.wj.mapper.RoleMapper;
import com.wj.mapper.UserInfoMapper;
import com.wj.mapper.UserMapper;
import com.wj.mapper.UserRoleMapper;
import com.wj.pojo.Role;
import com.wj.pojo.User;
import com.wj.pojo.UserInfo;
import com.wj.pojo.UserRole;
import com.wj.service.IUserService;
import com.wj.utils.RespBean;
import com.wj.vo.PagingVO;
import com.wj.vo.UserVo;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wj
 * @since 2022-05-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * 根据用户id查询它的角色
     * @param id
     * @return
     */
    @Override
    public List<Role> getUserWithRoleById(Integer id) {
        return roleMapper.getUserWithRoleById(id);
    }

    /**
     * 注册用户
     * @param user
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public RespBean registerUser(UserVo user) {
        // 新增用户信息
        UserInfo userInfo = UserInfo.builder()
                .email(user.getUsername())
                .nickname(CommonConstant.DEFAULT_NICK_NAME+IdWorker.getId())
                .avatar(CommonConstant.DEFAULT_AVATAR)
                .build();
        userInfoMapper.insert(userInfo);
        // 绑定用户角色
        UserRole userRole = UserRole.builder()
                .userId(userInfo.getId())
                .roleId(RoleEnum.USER.getRoleId())
                .build();
        userRoleMapper.insert(userRole);
        // 新增用户账号
        User userAuth = User.builder()
                .userInfoId(userInfo.getId())
                .username(user.getUsername())
                .password(passwordEncoder.encode(user.getPassword()))
                .loginType(LoginTypeEnum.FROM_LOGIN.getType())
//                .createTime(LocalDateTime.now())
                .build();
        userMapper.insert(userAuth);
        return RespBean.success("注册成功");
    }

    /**
     * 分页并根据关键字查询用户信息
     * @param pagingVo
     * @return
     */
    @Override
    public RespBean getPagingUsers(PagingVO pagingVo) {
        Integer count= userMapper.selectCountUser(pagingVo);
        if (count > 0){
            pagingVo.setStartPageSize((pagingVo.getCurrentPage()-1) * pagingVo.getPageSize());
            List<UserInfoDTO> userInfoDTOS = userMapper.getPagingUsers(pagingVo);
            return RespBean.success("成功",
                    PagingDTO.builder().count(count).object(userInfoDTOS).build());
        }
        return RespBean.error();
    }
}
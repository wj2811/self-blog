package com.wj.vo;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author wj
 * @Date 2022/5/30 21:25
 * @Description: TODO 分页vo
 * @Version 1.0
 */
@Data
@Builder
public class PagingVO {

    //当前页
    @NotNull(message = "页码不能为空")
    private Integer currentPage;

    //起始位置
    private Integer startPageSize;

    //一页多少条
    @NotNull(message = "一页数量不能为空")
    private Integer pageSize;

    //搜索关键字
    private String keyWords;
}

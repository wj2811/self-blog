import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    collapse: false,
    tabList: [{ name: "首页", path: "/" }],
    userId: null,
    roleList: null,
    avatar: null,
    nickname: null,
    intro: null,
    webSite: null,
    menuList: [],
    loading:false,
    theme:{
      body:"",
      side:"",
      head:""
    }
  },
  getters: {
  },
  mutations: {
    upTheme(state,theme){
      state.theme=theme
    },
    loading(state){
      state.loading=!state.loading
    },
    logout(state){
      sessionStorage.clear()
      state.menuList=[]
      state.tabList=[{ name: "首页", path: "/" }]
    },
    resetTab(state){
      state.tabList=[{ name: "首页", path: "/" }]
    },
    login(state, user) {
      state.userId = user.id;
      state.roleList = user.roleList;
      state.avatar = user.avatar;
      state.nickname = user.nickname;
      state.intro = user.intro;
      state.webSite = user.webSite;
    },
    saveMenuList(state, MenuList) {
      state.menuList = MenuList;
    },
    trigger(state) {
      state.collapse = !state.collapse;
    },
    saveTab(state, tab) {
      let tempTabList = JSON.parse(sessionStorage.getItem("tabList"))
      sessionStorage.removeItem("tabList")
      if(tempTabList){
        state.tabList=tempTabList
      }
      if (state.tabList.findIndex(item => item.path === tab.path) == -1) {
        state.tabList.push({ name: tab.name, path: tab.path });
      }
    },
    removeTab(state,tab){
      state.tabList.splice(state.tabList.findIndex(item=> item.name == tab.name),1)
    }
  },
  actions: {
  },
  modules: {
  }
})

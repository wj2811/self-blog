package com.wj.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @Author wj
 * @Date 2022/5/30 23:00
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
public class PagingDTO {

    //总用户数量
    private Integer count;

    //返回分页数据
    private Object object;
}

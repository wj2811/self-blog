package com.wj.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author wj
 * @Date 2022/5/29 10:39
 * @Description: TODO 角色
 * @Version 1.0
 */
@Getter
@AllArgsConstructor
public enum RoleEnum {
    ADMIN("管理员",1,"admin"),
    USER("用户",2,"user"),
    TEST("测试",3,"test"),
    ;
    private final String roleName;
    private final Integer roleId;
    private final String roleLabel;
}

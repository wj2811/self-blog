# self-blog

#### 介绍
自己的个人博客项目，欢迎大家相互学习，有很多功能没有完善，继续慢慢改动中
[前端预览](http://120.24.182.34/)

#### 软件架构
软件架构说明
### 后端：

> 1. springboot：web框架
> 2. springsecurity：安全框架
> 3. mybatis-plus：连接数据库框架
> 4. maven：依赖管理
> 5. fastDFS：文件存储系统
> 6. docker：部署服务用
> 7. Mysql：数据库连接工具

### 前端：

> 1. vue：这个就不用说了
> 2. vuex：网页存储数据，刷新无
> 3. vue-router	vue的路由
> 4. axios：网络请求
> 5. nprogress 加载条
> 6. vuetify	element	ui框架
> 7. stylus：css样式，可以css做动态动画
> 8. animation.css：css动态过度动画
> 9. markdown-it：解析markdown去掉标签
> 10. mavon-editor：markdown编辑器工具，写文章用


#### 安装教程

1.  npm 安装vue依赖就行，博客首页是blog，后台是admin
2.  后端改动mysql连接，导入数据库，还用了fastDFS保存文件，需要提前安装好
3.  现在不太完善，如有问题请联系作者

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

package com.wj.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author wj
 * @since 2022-05-31
 */
@Getter
@Setter
@TableName("t_article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 作者
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 作者昵称
     */
    @TableField(value = "nickname",exist = false)
    private String nickname;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空")
    @Size(message = "标题不能为空", min = 1)
    @TableField("article_title")
    private String articleTitle;

    /**
     * 分类
     */
    @NotBlank(message = "分类不能为空")
    @Size(message = "分类不能为空", min = 1)
    @TableField("category_name")
    private String categoryName;

    /**
     * 私有 0否 1是
     */
    @TableField("is_private")
    private Boolean isPrivate;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空")
    @Size(message = "内容不能为空", min = 1)
    @TableField("article_content")
    private String articleContent;

    /**
     * 发表时间
     */
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "Asia/Shanghai")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "Asia/Shanghai")
    private LocalDateTime updateTime;

    /**
     * 草稿 0否 1是
     */
    @TableField("is_draft")
    private Boolean isDraft;

    /**
     * 删除 0否 1是
     */
    @TableField("is_delete")
    private Boolean isDelete;


}

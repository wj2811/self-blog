package com.wj.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @Author wj
 * @Date 2022/5/29 9:13
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {

    @Size(message = "用户名不能小于8位",min = 8)
    private String username;
    @Size(message = "密码不能小于6位",min = 6)
    private String password;
    @NotBlank(message = "验证码不能为空")
    private String captcha;
}

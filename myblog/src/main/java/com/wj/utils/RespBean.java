package com.wj.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author wj
 * @Date 2022/5/26 10:21
 * @Description: TODO 公共返回类
 * @Version 1.0
 */
@Data
@AllArgsConstructor
public class RespBean {
    private Integer code;
    private String message;
    private Object data;

    /**
     * 自定义
     * @param message
     * @return
     */
    public static RespBean custom(Integer code,String message,Object obj){
        return new RespBean(code,message,obj);
    }

    /**
     * 返回成功的信息
     * @return
     */
    public static RespBean success(){
        return new RespBean(RespCode.SUCCESS.getCode(),RespCode.ERROR.getMessage(),null);
    }
    public static RespBean success(Object obj){
        return new RespBean(RespCode.SUCCESS.getCode(),RespCode.SUCCESS.getMessage(),obj);
    }
    public static RespBean success(String message){
        return new RespBean(RespCode.SUCCESS.getCode(),message,null);
    }
    public static RespBean success(String message, Object obj){
        return new RespBean(RespCode.SUCCESS.getCode(),message,obj);
    }

    /**
     * 返回失败信息
     * @return
     */
    public static RespBean error(){
        return new RespBean(RespCode.ERROR.getCode(), RespCode.ERROR.getMessage(),null);
    }
    public static RespBean error(String message){
        return new RespBean(RespCode.ERROR.getCode(), message,null);
    }
    public static RespBean error(Object obj){
        return new RespBean(RespCode.ERROR.getCode(), RespCode.ERROR.getMessage(), obj);
    }
    public static RespBean error(String message, Object obj){
        return new RespBean(RespCode.ERROR.getCode(), message,obj);
    }
}

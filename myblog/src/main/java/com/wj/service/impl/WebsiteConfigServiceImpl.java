package com.wj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wj.config.CommonConstant;
import com.wj.pojo.WebsiteConfig;
import com.wj.mapper.WebsiteConfigMapper;
import com.wj.service.IWebsiteConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wj.utils.RespBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wj
 * @since 2022-06-12
 */
@Service
public class WebsiteConfigServiceImpl extends ServiceImpl<WebsiteConfigMapper, WebsiteConfig> implements IWebsiteConfigService {

    @Resource
    private WebsiteConfigMapper websiteConfigMapper;

    /**
     * 获取用户首页进入的配置
     * @return
     */
    @Override
    public RespBean getWebsiteConfig() {
        WebsiteConfig websiteConfig = websiteConfigMapper.selectOne(
                new QueryWrapper<WebsiteConfig>().eq("id", CommonConstant.DEFAULT_WEBSITE_CONFIG)
        );
        if (Objects.nonNull(websiteConfig)){
            return RespBean.success(websiteConfig);
        }
        return RespBean.error();
    }
}

import Vue from 'vue'
import VueRouter from 'vue-router'
import Test from "@/views/Test";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: resolve=>{
      require(["../views/home/Home"],resolve)}
  },
  {
    path: '/article',
    name: 'article',
    component: resolve=>{
      require(["../views/article/Article"],resolve)}
  },
  {
    path: '/test',
    name: 'test',
    component:Test
  }
]

const router = new VueRouter({
  routes,
  mode:'history'
})

export default router

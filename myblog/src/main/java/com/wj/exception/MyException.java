package com.wj.exception;


import com.wj.utils.RespCode;

/**
 * @Author wj
 * @Date 2022/5/26 11:15
 * @Description: TODO 自定义异常类
 * @Version 1.0
 */
public class MyException extends RuntimeException{
    private Integer code;
    private String message;

    public MyException(Integer code,String message){
        this.code=code;
        this.message=message;
    }

    public MyException(RespCode respCode){
        this.code= respCode.getCode();
        this.message= respCode.getMessage();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.wj.service;

import com.wj.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wj.utils.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-05-27
 */
public interface IMenuService extends IService<Menu> {

    /**
     * 根据用户id查询它的菜单
     * @param id
     * @return
     */
    RespBean getMenusByUserId(Integer id);
}

package com.wj.mapper;

import com.wj.pojo.WebsiteConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-06-12
 */
public interface WebsiteConfigMapper extends BaseMapper<WebsiteConfig> {

}

package com.wj.controller;


import com.wj.pojo.UserInfo;
import com.wj.service.IUserInfoService;
import com.wj.utils.RespBean;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wj
 * @since 2022-05-29
 */
@RestController
@RequestMapping("/user-info")
public class UserInfoController {
}

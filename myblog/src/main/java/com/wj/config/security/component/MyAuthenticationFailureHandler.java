package com.wj.config.security.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wj.utils.RespBean;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest res, HttpServletResponse resp, AuthenticationException e) throws IOException, ServletException {
        resp.setContentType("application/json;charset=UTF-8");
        if (e instanceof DisabledException){
            String successRes = new ObjectMapper().writeValueAsString(RespBean.error("账号已被禁用，请联系管理员"));
            resp.getWriter().print(successRes);
            return;
        }
        String failRes = new ObjectMapper().writeValueAsString(RespBean.error("密码或用户名错误"));
        resp.getWriter().print(failRes);
    }
}

package com.wj.mapper;

import com.wj.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-05-26
 */
public interface RoleMapper extends BaseMapper<Role> {

    List<Role> getUserWithRoleById(Integer id);
}

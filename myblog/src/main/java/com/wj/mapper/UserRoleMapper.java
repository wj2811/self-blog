package com.wj.mapper;

import com.wj.pojo.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-05-29
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}

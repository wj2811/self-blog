package com.wj.service.impl;

import com.wj.pojo.Menu;
import com.wj.mapper.MenuMapper;
import com.wj.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wj.utils.RespBean;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wj
 * @since 2022-05-27
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {
    @Resource
    private MenuMapper menuMapper;

    @Override
    public RespBean getMenusByUserId(Integer id) {
        List<Menu> menuList = menuMapper.getMenusByUserId(id);
        if (!ObjectUtils.isEmpty(menuList)){
            return RespBean.success("成功",menuList);
        }
        return RespBean.error();
    }
}

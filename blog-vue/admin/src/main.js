import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import api from '@/api'
import axios from "axios";
import VueAxios from "vue-axios";
import {postRequest} from "@/utils/requestUtil";
import {getRequest} from "@/utils/requestUtil";
import {deleteRequest} from "@/utils/requestUtil";
import {putRequest} from "@/utils/requestUtil";
import NProgress from "nprogress";
import 'nprogress/nprogress.css'
import {initMenu} from "@/utils/menu";
import './assets/css/iconfont.css';
import commonUtil from "@/utils/commonUtil";
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

Vue.config.productionTip = false
Vue.use(mavonEditor)
Vue.use(ElementUI);
Vue.prototype.api=api
Vue.use(VueAxios, axios);
Vue.prototype.postRequest=postRequest
Vue.prototype.getRequest=getRequest
Vue.prototype.putRequest=putRequest
Vue.prototype.deleteRequest=deleteRequest
Vue.prototype.commonUtil=commonUtil

//进度条配置
NProgress.configure({
  spinnerSelector: '[role="spinner"]',
  easing: "ease", // 动画方式
  speed: 500, // 递增进度条的速度
  showSpinner: true, // 是否显示加载ico
  trickleSpeed: 200, // 自动递增间隔
  minimum: 0.3 // 初始化时的最小百分比
});

//路由跳转前
router.beforeEach((to, from, next) => {
  NProgress.start();
  console.log("to:",to.path)
  let user = JSON.parse(sessionStorage.getItem('user'))
  if (user){
    store.commit("login",user)
    initMenu()
    next()
  }else if(to.path == "/login"){
    next()
  }else{
    next({path:"/login",query:{to:to.path}})
  }
})

//路由跳转后
router.afterEach((to, from) => {
  NProgress.done()
})

export default new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


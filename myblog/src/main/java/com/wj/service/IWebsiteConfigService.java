package com.wj.service;

import com.wj.pojo.WebsiteConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wj.utils.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-06-12
 */
public interface IWebsiteConfigService extends IService<WebsiteConfig> {

    /**
     * 获取用户首页进入的配置
     * @return
     */
    RespBean getWebsiteConfig();
}

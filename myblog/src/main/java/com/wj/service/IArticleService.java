package com.wj.service;

import com.wj.pojo.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wj.utils.RespBean;
import com.wj.vo.PagingVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-05-31
 */
public interface IArticleService extends IService<Article> {

    /**
     * 添加文章
     * @param article
     * @return
     */
    RespBean addArticle(Article article);

    /**
     * 获取分页的文章
     * @param pagingVO
     * @return
     */
    RespBean getPagingArticle(PagingVO pagingVO);

    /**
     * 首页展示文章
     * @param pagingVO
     * @return
     */
    RespBean getHomeArticles(PagingVO pagingVO);

    /**
     * 根据id获取对应文章
     * @param id
     * @return
     */
    RespBean getArticleById(Integer id);
}

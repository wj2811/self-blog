export default {
    login:"/api/login",
    logout:"/api/logout",
    menu:"/api/menu/",
    user:"/api/user/",
    userInfo:"/api/user-info/",
    article:"/api/article/"
}
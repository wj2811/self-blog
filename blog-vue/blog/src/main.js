import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import '@/assets/css/scrollbar.css'
import '@/assets/icon/iconfont.css'
import '@/assets/css/index.css'
import 'animate.css';
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import api from './api'
import axios from "axios";
import VueAxios from "vue-axios";
import {postRequest} from "@/utils/requestUtil";
import {getRequest} from "@/utils/requestUtil";
import {deleteRequest} from "@/utils/requestUtil";
import {putRequest} from "@/utils/requestUtil";
import NProgress from "nprogress";
import 'nprogress/nprogress.css'
import commonUtil from "@/utils/commonUtil";
import '@/assets/css/tocbot.css'

//导入代码高亮文件
import hljs from 'highlight.js'
//导入代码高亮样式
import 'highlight.js/styles/vs2015.css'
import {use} from "marked";

Vue.config.productionTip = false
Vue.prototype.api = api
Vue.use(VueAxios, axios)
Vue.use(mavonEditor)
Vue.prototype.postRequest = postRequest
Vue.prototype.getRequest = getRequest
Vue.prototype.putRequest = putRequest
Vue.prototype.deleteRequest = deleteRequest

//进度条配置
NProgress.configure({
    spinnerSelector: '[role="spinner"]',
    easing: "ease", // 动画方式
    speed: 500, // 递增进度条的速度
    showSpinner: true, // 是否显示加载ico
    trickleSpeed: 200, // 自动递增间隔
    minimum: 0.3 // 初始化时的最小百分比
});

//自定义一个代码高亮指令
Vue.directive('highlight',function (el) {
    let highlight = el.querySelectorAll('pre code');
    highlight.forEach((block)=>{
        hljs.highlightBlock(block)
    })
})

//路由跳转前
router.beforeEach((to, from, next) => {
    NProgress.start();
    // console.log("to:", to.path)
    if (store.state.user == ""){
        initUser()
    }
    next()
})

//路由跳转后
router.afterEach((to, from) => {
    NProgress.done()
})

//初始化网站信息
function initUser() {
    getRequest(api.websiteConfig).then(res => {
        if (res){
            let user = JSON.parse(res.data.config)
            user.updateDay = parseInt((new Date().getTime()-Date.parse(user.updateTime)) / 86400 /1000)
            user.runTime = commonUtil.formatTimer(user.runTime)
            store.commit("setUser",user)
        }
    })
}

let vue = new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')

export default vue
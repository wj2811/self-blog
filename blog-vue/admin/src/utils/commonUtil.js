import that from '@/main'

export default {
    hintMessage(message,title="提示",color="teal"){
        const h = that.$createElement;
        that.$notify({
            title: title,
            duration:3000,
            message: h('i', { style: `color: ${color}`}, message)
        });
    },
    copyObjet(obj){
        return JSON.parse(JSON.stringify(obj))
    },
}
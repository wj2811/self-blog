package com.wj.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author wj
 * @since 2022-06-12
 */
@Getter
@Setter
@TableName("t_website_config")
public class WebsiteConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 网站信息配置json格式
     */
    @TableField("config")
    private String config;

    /**
     * 创建配置时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 修改配置时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


}

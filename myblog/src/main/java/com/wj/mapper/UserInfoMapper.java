package com.wj.mapper;

import com.wj.dto.UserInfoDTO;
import com.wj.pojo.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-05-29
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}

package com.wj.config.security.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wj.utils.RespBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author wj
 * @Date 2022/5/28 15:57
 * @Description: TODO 注销成功处理
 * @Version 1.0
 */
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse resp, Authentication authentication) throws IOException, ServletException {
        resp.setContentType("application/json;charset=UTF-8");
        String str = new ObjectMapper().writeValueAsString(RespBean.success("注销成功"));
        resp.getWriter().print(str);
    }
}

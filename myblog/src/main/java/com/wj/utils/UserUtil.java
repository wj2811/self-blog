package com.wj.utils;

import com.wj.pojo.User;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @Author wj
 * @Date 2022/5/27 12:14
 * @Description: TODO
 * @Version 1.0
 */
public class UserUtil {
    /**
     * 获取当前角色
     * @return
     */
    public static User getCurrentUser(){
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

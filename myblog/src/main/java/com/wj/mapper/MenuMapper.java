package com.wj.mapper;

import com.wj.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wj.utils.RespBean;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-05-27
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> getMenusByUserId(Integer id);
}

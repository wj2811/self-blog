/*
 Navicat Premium Data Transfer

 Source Server         : 云
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 120.24.182.34:4417
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 15/06/2022 15:49:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '作者',
  `article_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类',
  `is_private` tinyint(1) NULL DEFAULT NULL COMMENT '私有 0否 1是',
  `article_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发表时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_draft` tinyint(1) NULL DEFAULT NULL COMMENT '草稿 0否 1是',
  `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` VALUES (3, 200, 'docker配置', 'docker', 0, '## docker配置\n\n### 安装docker\n\n> yum install docker\n>\n> 修改镜像加速\n>\n> 1. ```shell\n>    vim /etc/docker/daemon.json\n>    ```\n>\n> 2. ```\n>    {  \"registry-mirrors\": [\"https://wlj591yk.mirror.aliyuncs.com\"] }\n>    ```\n>\n> 3. ```\n>    systemctl restart docker\n>    ```\n\n### 创建docker统一挂载目录\n\n> mkdir /opt/docker_mount\n\n### 开启启动容器\n\n> docker update --restart=always tracker\n> docker update --restart=always storage\n\n## 能够用得到的一些命令：\n\n```shell\ndocker中 启动所有的容器命令\n[root@VM-8-7-centos logs]# docker start $(docker ps -a | awk \'{ print $1}\' | tail -n +2)\n\ndocker中 关闭所有的容器命令\n[root@VM-8-7-centos logs]# docker stop $(docker ps -a | awk \'{ print $1}\' | tail -n +2)\n\ndocker中 删除所有的容器命令\n[root@VM-8-7-centos logs]# docker rm $(docker ps -a | awk \'{ print $1}\' | tail -n +2)\n\ndocker中 删除所有的镜像\n[root@VM-8-7-centos logs]# docker rmi $(docker images | awk \'{print $3}\' |tail -n +2)\n```\n\n## docker部署Tomcat\n\n> docker run  --name tomcat -p 9191:8080 \\\n> -v /opt/docker_mount/tomcat/logs:/usr/local/tomcat/logs \\\n> -v /opt/docker_mount/tomcat/webapps:/usr/local/tomcat/webapps \\\n> --privileged=true -d tomcat\n\n## docker部署nginx\n\n1. 下载镜像\n\n   ```\n   docker pull nginx\n   ```\n\n2. 创建这些文件夹\n\n   ```\n   mkdir -p /opt/docker_mount/nginx/\n   ```\n\n3. 启动一个复制文件**cp后面的是容器id**（ docker run -d -p 80:80 nginx ）\n\n   ```\n   docker cp 4adc8dee3dee:/etc/nginx/nginx.conf /opt/docker_mount/nginx/\n   docker cp 4adc8dee3dee:/etc/nginx/conf.d  /opt/docker_mount/nginx/conf\n   docker cp 4adc8dee3dee:/usr/share/nginx/html/  /opt/docker_mount/nginx/html/\n   docker cp 4adc8dee3dee:/var/log/nginx/  /opt/docker_mount/nginx/logs/\n   ```\n\n4. 再把运行的nginx容器删除\n\n   ```\n   docker ps 先查看\n   docker rm -f 容器id\n   ```\n\n5. 配置挂载目录启动\n\n   ```\n   docker run  --name nginx -p 80:80 \\\n      -v /opt/docker_mount/nginx/nginx.conf:/etc/nginx/nginx.conf \\\n      -v /opt/docker_mount/nginx/logs:/var/log/nginx/logs \\\n      -v /opt/docker_mount/nginx/html:/usr/share/nginx/html \\\n      -v /opt/docker_mount/nginx/conf:/etc/nginx/conf.d \\\n      -e TZ=Asia/Shanghai \\\n      --privileged=true -d nginx\n   ```\n\n6. 配置default.conf文件\n\n   ```\n   server {\n       listen       80;\n       listen  [::]:80;\n       server_name  localhost;\n   \n       #access_log  /var/log/nginx/host.access.log  main;\n   \n       location / {\n           root   /usr/share/nginx/html;\n           index  index.html index.htm;\n       }\n   \n       location ^~/api/ {\n           proxy_set_header Host $http_host;\n           proxy_set_header X-Real-IP $remote_addr;\n           proxy_set_header REMOTE-HOST $remote_addr;\n           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n           proxy_pass http://172.24.53.166:9191/; #直接写ip也行\n       }\n   \n   ```\n\n\n## docker安装fastDFS\n\n### 查找Docker Hub上的fastdfs镜像：\n\n查找fastdfs镜像\n\n> docker search fastdfs\n\n拉取最新版本\n\n> docker pull delron/fastdfs\n\n查看镜像\n\n> docker images\n\n### 放行这些端口，如果是云服务器记得配置安全规则放行\n\n> firewall-cmd --zone=public  --permanent --add-port=8888/tcp\n> firewall-cmd --zone=public  --permanent --add-port=22122/tcp\n> firewall-cmd --zone=public  --permanent --add-port=23000/tcp\n>\n> 重启防火墙\n> systemctl restart firewalld\n\n使用docker镜像构建tracker容器（跟踪服务器，起到调度的作用）\n\n> docker run -dti --network=host --name tracker -v /opt/docker_mount/fdfs/tracker:/var/fdfs -v /etc/localtime:/etc/localtime delron/fastdfs tracker\n\n使用docker镜像构建storage容器（存储服务器，提供容量和备份服务）\n\n **注意：**TRACKER_SERVER=本机的ip地址:22122 本机ip地址不要使用127.0.0.1，如果是云服务器的话用公网的ip\n\n> docker run -dti  --network=host --name storage -e TRACKER_SERVER=120.24.182.34:22122 -v /opt/docker_mount/fdfs/storage:/var/fdfs  -v /etc/localtime:/etc/localtime  delron/fastdfs storage\n\n### 如果ip设置错了，请看下面的步骤\n\n```shell\n查看当前运行下的镜像进程\n[root@VM-8-7-centos logs]# docker ps -a\n停止storage这个镜像\n[root@VM-8-7-centos logs]# docker stop [填写你的storage的CONTAINER ID] \n删除storage这个镜像\n[root@VM-8-7-centos logs]# docker stop [填写你的storage的CONTAINER ID] \n之后再重新新建\n```\n\n### 测试，上传一个文件\n\n进入storage容器\n\n> docker exec -it storage bash\n\n> fdfs_upload_file /etc/fdfs/client.conf http.conf	**http.conf**这个文件随意，想上传图片也可以\n>\n> group1/M00/00/00/rBg1pmKiAgeAUo5RAAADu664ZlM77.conf 出现这个说明成功了\n\n### 此时已上传至文件系统，并在执行该语句后返回存储的url：\n\n通过以上url加上你的，就可以访问了\n\n> http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKiAgeAUo5RAAADu664ZlM77.conf\n>\n> 前面是你的ip地址\n\n### 配置nginx（进入storage配置），**此步骤可以不操作**\n\n```shell\n进入容器查看storage：\n[root@VM-8-7-centos logs]# docker exec -it storage /bin/bash\n\n在/usr/local/nginx/conf目录下，修改nginx.conf文件\n[root@VM-8-7-centos conf]# cd /usr/local/nginx/conf\n\n--------------------------以下是nginx.conf文件--------------------------\n\n server {\n        listen       8888;\n        server_name  localhost;\n        location ~/group[0-9]/ {\n            ngx_fastdfs_module;\n        }\n        error_page   500 502 503 504  /50x.html;\n        location = /50x.html {\n            root html;\n        }\n    }\n\n--------------------------以上是nginx.conf文件--------------------------\n```', '2022-05-31 16:34:51', '2022-06-01 00:37:49', 0, 0);
INSERT INTO `t_article` VALUES (4, 200, '老八汉堡秘方', 'java', 0, '::: hljs-center\n\n**鸡屁股**\n\n:::\n', '2022-05-31 16:37:21', '2022-06-01 00:37:53', 0, 0);
INSERT INTO `t_article` VALUES (5, 200, '2022/6/1 上午12:47:41 文本编辑器2022/6/1 上午12:47:41 文本编辑器', '项目经历', 0, '# 终于基本搞完了，# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天# 终于基本搞完了，文本编辑器用了一天文本编辑器用了一天', '2022-05-31 16:49:27', '2022-05-31 16:49:27', 0, 0);
INSERT INTO `t_article` VALUES (6, 200, '有图片', '随笔', 0, '> 阿斯蒂芬\n![b703025357ec473290683bec0e7079e7.jpg](http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKWwh6ATTuqAAT4Rrwdw6s782.jpg)', '2022-06-01 01:40:32', '2022-06-01 01:40:32', 0, 0);
INSERT INTO `t_article` VALUES (11, 200, '测试删除默认值', '测试', 0, 'sadfasdf', '2022-06-01 05:11:32', '2022-06-01 05:11:32', 0, 0);

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单名',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `component` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单icon',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `order_num` tinyint NULL DEFAULT NULL COMMENT '排序',
  `parent_id` int NULL DEFAULT NULL COMMENT '父id',
  `is_disable` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用 0否1是',
  `is_hidden` tinyint(1) NULL DEFAULT 0 COMMENT '是否隐藏  0否1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, '首页', '/', '/home/Home.vue', 'icon-home', '2022-05-27 12:29:28', '2022-05-27 12:29:31', 1, NULL, 0, 0);
INSERT INTO `t_menu` VALUES (2, '用户管理', '/users', 'SideBar', 'icon-yonghu', '2022-05-27 11:20:42', '2022-05-27 11:20:45', 2, NULL, 0, 0);
INSERT INTO `t_menu` VALUES (3, '文章管理', '/article-submenu', 'SideBar', 'icon-16', '2021-01-25 20:43:07', '2021-01-25 20:43:09', 3, NULL, 0, 0);
INSERT INTO `t_menu` VALUES (4, '消息管理', '/message-submenu', 'SideBar', 'icon-xiaoxi', '2021-01-25 20:44:17', '2021-01-25 20:44:20', 4, NULL, 0, 0);
INSERT INTO `t_menu` VALUES (5, '系统管理', '/system-submenu', 'SideBar', 'icon-xitong', '2021-01-25 20:45:57', '2021-01-25 20:45:59', 5, NULL, 0, 0);
INSERT INTO `t_menu` VALUES (6, '写文章', '/articles-add', '/article/Article.vue', 'icon-tianjiawenzhang', '2021-01-26 14:30:48', '2021-01-26 14:30:51', 6, 3, 0, 0);
INSERT INTO `t_menu` VALUES (9, '文章列表', '/article-list', '/article/ArticleList.vue', 'icon-liebiao', '2021-01-26 14:32:13', '2021-01-26 14:32:16', 7, 3, 0, 0);
INSERT INTO `t_menu` VALUES (10, '修改文章', '/article-update/*', '/article/ArticleUpdate.vue', 'icon-liebiao', '2021-01-26 14:32:13', '2021-01-26 14:32:16', 7, 3, 0, 0);
INSERT INTO `t_menu` VALUES (11, '用户列表', '/users-list', '/user/UserList.vue', 'icon-liebiao', '2022-05-29 15:19:49', '2022-05-29 15:19:53', NULL, 2, 0, 0);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名',
  `role_label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_disable` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用  0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, '管理员', 'admin', '2021-01-11 17:21:57', '2021-03-20 23:27:55', 0);
INSERT INTO `t_role` VALUES (2, '用户', 'user', '2021-01-11 20:17:05', '2021-03-16 23:20:20', 0);
INSERT INTO `t_role` VALUES (3, '测试', 'test', '2021-01-11 20:17:23', '2021-03-16 23:41:59', 0);

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu_id` int NULL DEFAULT NULL COMMENT '菜单id',
  `role_id` int NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (1, 1, 1);
INSERT INTO `t_role_menu` VALUES (2, 2, 1);
INSERT INTO `t_role_menu` VALUES (3, 3, 1);
INSERT INTO `t_role_menu` VALUES (4, 4, 1);
INSERT INTO `t_role_menu` VALUES (5, 5, 1);
INSERT INTO `t_role_menu` VALUES (6, 6, 1);
INSERT INTO `t_role_menu` VALUES (7, 7, 1);
INSERT INTO `t_role_menu` VALUES (9, 11, 1);
INSERT INTO `t_role_menu` VALUES (10, 6, 2);
INSERT INTO `t_role_menu` VALUES (11, 9, 2);
INSERT INTO `t_role_menu` VALUES (12, 3, 2);
INSERT INTO `t_role_menu` VALUES (13, 1, 2);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_info_id` int NOT NULL COMMENT '用户信息id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `login_type` tinyint(1) NOT NULL COMMENT '登录类型',
  `ip_addr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户登录ip',
  `ip_source` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ip来源',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `last_login_time` timestamp NULL DEFAULT NULL COMMENT '上次登录时间',
  `is_enable` tinyint(1) NULL DEFAULT 1 COMMENT '是否激活',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 214 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (200, 200, 'root', '$2a$10$8/oChN2VI.GYRkqpi5rU.OQw/aOasoqsQ3Kpb9Dwgdig5W5.eseSq', 0, '110.53.5.214', '湖南省娄底市娄星区 联通', '2022-05-26 19:08:44', '2022-06-15 03:08:06', 1);
INSERT INTO `t_user` VALUES (201, 201, '11111111', '$2a$10$8NxJlONC/b7VKgCAf8ovmuWlek1hTwq/xzj2KOnCJhzHnYMxul4vS', 0, '127.0.0.1', '', '2022-05-29 06:56:24', '2022-05-30 18:49:10', 1);
INSERT INTO `t_user` VALUES (202, 202, 'q1234567', '$2a$10$oHSLjeHrXtdeQZVrnvk8D.UMBji5brps66Yg3.hifjkfZU5S6Djl2', 0, NULL, NULL, '2022-05-30 10:27:04', NULL, 1);
INSERT INTO `t_user` VALUES (203, 203, 'asdfasdf', '$2a$10$Dt.rJA08iB0dkI7EitvEj.OX9RZjR6SfvnWMHgBOSNxthe59VAOiy', 0, '169.254.205.192', '保留地址', '2022-05-30 10:29:47', '2022-05-30 22:55:46', 0);
INSERT INTO `t_user` VALUES (204, 204, 'a1234567', '$2a$10$qCloaCAzrNpAb1h80Pzr6Ocbv7lL7tatvIiylT/oQJt2DJmJ3KRUu', 0, NULL, NULL, '2022-05-30 10:30:45', NULL, 1);
INSERT INTO `t_user` VALUES (205, 205, 'b1234567', '$2a$10$XRgwvXyB27ZhwokzIDKJFuDbzu4b02ez9Pu3ME/tfM40dBYuznWhG', 0, NULL, NULL, '2022-05-30 10:30:50', NULL, 1);
INSERT INTO `t_user` VALUES (206, 206, 'c1234567', '$2a$10$DlpyF95xUYbkxOuarRgLieLaul5s8KDf8EIkE0ud.XIuc/lSSnYpy', 0, NULL, NULL, '2022-05-30 10:30:53', NULL, 1);
INSERT INTO `t_user` VALUES (207, 207, 'd1234567', '$2a$10$2qk2qeaEws.y7oWYgrpBXO1sep5c24jR64F3NAhlkSk9OmvWQIVbO', 0, NULL, NULL, '2022-05-30 10:30:56', NULL, 1);
INSERT INTO `t_user` VALUES (208, 208, 'e1234567', '$2a$10$r2Ouiwxo..MS256dtkZGf.8rwPRhHknsyv8YQMP.Y2mlCutGFaEgq', 0, NULL, NULL, '2022-05-30 10:30:59', NULL, 0);
INSERT INTO `t_user` VALUES (209, 209, 'f1234567', '$2a$10$hpNICgPatFRXZ/cQ/Ib4p.TrfdKhxlRbYx7vTrIKCdJBRX53wPlBC', 0, NULL, NULL, '2022-05-30 10:31:06', NULL, 0);
INSERT INTO `t_user` VALUES (210, 210, 'g1234567', '$2a$10$gAKUFiZfiq8CdA12Q1rw2ufDM3r7D3sF3Ia8oXLORHhcn6Pd.DGi2', 0, NULL, NULL, '2022-05-30 10:31:10', NULL, 0);
INSERT INTO `t_user` VALUES (211, 211, 'h1234567', '$2a$10$2N58Zru3kjEshhXCNSX8huCVVtQ3Ljd.xKcnmb/iBpSXBAMxrDMCm', 0, NULL, NULL, '2022-05-30 10:31:20', NULL, 0);
INSERT INTO `t_user` VALUES (213, 213, 'n1234567', '$2a$10$fFxexYaPqCnctkYx2D67ke1t.i4IYPcGIlheQqcvJXJpuP7esECGC', 0, NULL, NULL, '2022-05-30 11:48:31', NULL, 1);

-- ----------------------------
-- Table structure for t_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_user_info`;
CREATE TABLE `t_user_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱号',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户简介',
  `web_site` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '个人网站',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `is_disable` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 214 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_user_info
-- ----------------------------
INSERT INTO `t_user_info` VALUES (200, NULL, '管理员', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKiqNGANC0cAABSRM45aq4564.png', '发表你的第一篇博客吧', 'https://www.talkxj.com', '2020-06-29 10:48:18', '2021-03-20 22:10:33', 0);
INSERT INTO `t_user_info` VALUES (201, '11111111', '新用户1530805237299113985', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-29 06:56:24', NULL, 0);
INSERT INTO `t_user_info` VALUES (202, 'q1234567', '新用户1531220635165491202', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:27:04', NULL, 0);
INSERT INTO `t_user_info` VALUES (203, 'asdfasdf', '新用户1531221320544124929', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:29:47', NULL, 0);
INSERT INTO `t_user_info` VALUES (204, 'a1234567', '新用户1531221564581314562', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:30:45', NULL, 0);
INSERT INTO `t_user_info` VALUES (205, 'b1234567', '新用户1531221582553907202', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:30:49', NULL, 0);
INSERT INTO `t_user_info` VALUES (206, 'c1234567', '新用户1531221597645012993', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:30:53', NULL, 0);
INSERT INTO `t_user_info` VALUES (207, 'd1234567', '新用户1531221609066102785', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:30:56', NULL, 0);
INSERT INTO `t_user_info` VALUES (208, 'e1234567', '新用户1531221621711929345', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:30:59', NULL, 0);
INSERT INTO `t_user_info` VALUES (209, 'f1234567', '新用户1531221651466321922', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:31:06', NULL, 0);
INSERT INTO `t_user_info` VALUES (210, 'g1234567', '新用户1531221665819230210', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:31:09', NULL, 0);
INSERT INTO `t_user_info` VALUES (211, 'h1234567', '新用户1531221710522122242', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 10:31:20', NULL, 0);
INSERT INTO `t_user_info` VALUES (213, 'n1234567', '新用户1531241133148565506', 'http://120.24.182.34:8888/group1/M00/00/00/rBg1pmJuFPKAbbVmAAGlffRZSHs629.jpg', NULL, NULL, '2022-05-30 11:48:31', NULL, 0);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `role_id` int NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 234 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (219, 200, 1);
INSERT INTO `t_user_role` VALUES (220, 200, 2);
INSERT INTO `t_user_role` VALUES (221, 201, 2);
INSERT INTO `t_user_role` VALUES (222, 202, 2);
INSERT INTO `t_user_role` VALUES (223, 203, 2);
INSERT INTO `t_user_role` VALUES (224, 204, 2);
INSERT INTO `t_user_role` VALUES (225, 205, 2);
INSERT INTO `t_user_role` VALUES (226, 206, 2);
INSERT INTO `t_user_role` VALUES (227, 207, 2);
INSERT INTO `t_user_role` VALUES (228, 208, 2);
INSERT INTO `t_user_role` VALUES (229, 209, 2);
INSERT INTO `t_user_role` VALUES (230, 210, 2);
INSERT INTO `t_user_role` VALUES (231, 211, 2);
INSERT INTO `t_user_role` VALUES (233, 213, 2);

-- ----------------------------
-- Table structure for t_website_config
-- ----------------------------
DROP TABLE IF EXISTS `t_website_config`;
CREATE TABLE `t_website_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '网站信息配置json格式',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建配置时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改配置时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_website_config
-- ----------------------------
INSERT INTO `t_website_config` VALUES (1, '{\"nickname\":\"君役\",\"intro\":\"因为喜欢你，所以想继续相信下去。\",\"version\":\"0.1.1\",\"avatar\":\"http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKpTTyAaeNyAABSRM45aq4786.png\",\"bj\":\"background: url(http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKowIaAbhI0AAPLtm4qgA0647.png)\",\"runTime\":\"Sat Jun 11 2022 18:02:16 GMT+0800\",\"updateTime\":\"Sat Jun 11 2022 18:02:16 GMT+0800\",\"updateDay\":1,\"relations\":[{\"name\":\"gitee\",\"iconfont\":\"icon-gitee-fill-round\",\"hint\":\"已开源\",\"path\":\"https://gitee.com/wj2811/self-blog\"},{\"name\":\"qq\",\"iconfont\":\"icon-logo-qq\",\"hint\":\"2811724301\",\"path\":\"http://wpa.qq.com/msgrd?v=3&uin=2811724301&site=qq&menu=yes\"},{\"name\":\"github\",\"iconfont\":\"icon-github\",\"hint\":\"暂未开放\",\"path\":\"\"}]}', '2022-06-12 21:19:52', '2022-06-12 21:19:56');

SET FOREIGN_KEY_CHECKS = 1;

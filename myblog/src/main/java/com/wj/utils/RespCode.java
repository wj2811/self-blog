package com.wj.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author wj
 * @Date 2022/5/26 10:31
 * @Description: TODO
 * @Version 1.0
 */
@Getter
@AllArgsConstructor
public enum RespCode {
    GLOBAL_ERROR(1001,"系统繁忙"),
    SUCCESS(2000,"成功"),
    ERROR(5000,"失败"),
    USER_DISABLE(5002,"账号已被禁用，请联系管理员")
    ;

    private Integer code;
    private String message;

}

package com.wj.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author wj
 * @Date 2022/5/29 16:36
 * @Description: TODO 用户信息dto类
 * @Version 1.0
 */
@Data
public class UserInfoDTO {

    //用户id
    private Integer id;

    //登录类型
    private Integer loginType;

    //ip地址
    private String ipAddress;

    //ip位置
    private String ipSource;

    //创建时间
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "Asia/Shanghai")
    private LocalDateTime createTime;

    //最后登录时间
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "Asia/Shanghai")
    private LocalDateTime lastLoginTime;

    //邮箱
    private String email;

    //别名
    private String nickname;

    //头像
    private String avatar;

    //简介
    private String intro;

    //账号是否激活
    private Boolean isEnable;

//    //ip地址
//    private String ipAddr;
//
//    private String ipSource;
}

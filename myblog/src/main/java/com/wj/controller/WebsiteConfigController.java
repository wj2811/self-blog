package com.wj.controller;


import com.wj.service.IWebsiteConfigService;
import com.wj.utils.RespBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wj
 * @since 2022-06-12
 */
@RestController
@RequestMapping("/website-config")
public class WebsiteConfigController {

    @Resource
    private IWebsiteConfigService websiteConfigService;

    /**
     * 获取用户首页进入的配置
     * @return
     */
    @GetMapping("/")
    public RespBean getWebsiteConfig(){
        return websiteConfigService.getWebsiteConfig();
    }
}

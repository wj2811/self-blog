package com.wj.config;

/**
 * @Author wj
 * @Date 2022/6/12 22:44
 * @Description: TODO 公告常量类
 * @Version 1.0
 */
public class CommonConstant {
    /*新用户默认头像*/
    public static final String DEFAULT_AVATAR = "http://120.24.182.34:8888/group1/M00/00/00/rBg1pmKiqNGANC0cAABSRM45aq4564.png";

    /*新用户默认名前缀*/
    public static final String DEFAULT_NICK_NAME = "新用户";

    /*用户网站默认获取配置id*/
    public static final int DEFAULT_WEBSITE_CONFIG = 1;
}

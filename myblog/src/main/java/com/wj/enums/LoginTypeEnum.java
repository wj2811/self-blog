package com.wj.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author wj
 * @Date 2022/5/29 10:48
 * @Description: TODO 登录方式
 * @Version 1.0
 */
@Getter
@AllArgsConstructor
public enum LoginTypeEnum {
    FROM_LOGIN(0,"普通登录");

    private Integer type;
    private String desc;
}

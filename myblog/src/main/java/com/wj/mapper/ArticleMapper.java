package com.wj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wj.pojo.Article;
import com.wj.vo.PagingVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2022-05-31
 */
public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> getPagingArticle(@Param("pagingVO") PagingVO pagingVO,@Param("id") Integer id);

    Integer selectCountArticle(PagingVO pagingVO);
}

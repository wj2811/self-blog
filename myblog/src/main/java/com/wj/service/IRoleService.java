package com.wj.service;

import com.wj.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-05-26
 */
public interface IRoleService extends IService<Role> {

}

package com.wj.exception.handle;

import com.wj.enums.ValidEnum;
import com.wj.exception.MyException;
import com.wj.utils.RespBean;
import com.wj.utils.RespCode;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

/**
 * @Author wj
 * @Date 2022/5/26 10:57
 * @Description: TODO 全局异常类
 * @Version 1.0
 */
@ControllerAdvice
public class GlobalException {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public RespBean exception(MethodArgumentNotValidException e){
        return RespBean.custom(ValidEnum.PARAMETER_ERROR.getCode(), e.getBindingResult().getFieldError().getDefaultMessage(),null);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public RespBean exception(Exception e){
        e.printStackTrace();
        return RespBean.custom(RespCode.GLOBAL_ERROR.getCode(), RespCode.GLOBAL_ERROR.getMessage(),null);
    }

    @ExceptionHandler(MyException.class)
    @ResponseBody
    public RespBean exception(MyException e){
        e.printStackTrace();
        return RespBean.custom(e.getCode(), e.getMessage(),null);
    }
}

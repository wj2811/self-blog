module.exports = {
  productionSourceMap: false,//打包压缩
  devServer: {
    port:8081,
    proxy: {
      "/api": {
        target: "http://localhost:8080",
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    },
    // disableHostCheck: true //配置项用于配置是否关闭用于 DNS 重绑定的 HTTP 请求的 HOST 检查
  },
  //对内部webpack配置更细粒度的更改
  // chainWebpack: config => {
  //   config.resolve.alias.set("@", resolve("src"));
  // }
};

// const path = require("path");
// function resolve(dir) {
//   return path.join(__dirname, dir);
// }

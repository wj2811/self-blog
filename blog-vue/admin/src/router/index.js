import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/login/Login";
import test1 from "@/views/test/test1";
import test2 from "@/views/test/test2";

Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/test1',
        name: 'test1',
        component: test1,
        children:[
            {
                path: '/test2',
                name: 'test2',
                component: test2
            }
        ]
    }
]

const router = new VueRouter({
    routes,
    mode: "history",
})

const createRouter = () =>
    new VueRouter({
        mode: "history",
        routes: routes
    });

export function resetRouter() {
    const newRouter = createRouter();
    router.matcher = newRouter.matcher;
}

export default router

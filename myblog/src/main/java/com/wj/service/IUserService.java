package com.wj.service;

import com.wj.pojo.Role;
import com.wj.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wj.utils.RespBean;
import com.wj.vo.PagingVO;
import com.wj.vo.UserVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-05-26
 */
public interface IUserService extends IService<User> {

    /**
     * 根据用户id查询它的角色
     * @param id
     * @return
     */
    List<Role> getUserWithRoleById(Integer id);


    /**
     * 注册用户
     * @param user
     * @return
     */
    RespBean registerUser(UserVo user);

    /**
     * 分页并根据关键字查询用户信息
     * @param pagingVo
     * @return
     */
    RespBean getPagingUsers(PagingVO pagingVo);
}

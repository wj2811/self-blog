package com.wj.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author wj
 * @Date 2022/5/29 9:54
 * @Description: TODO 接口验证数据枚举类
 * @Version 1.0
 */
@Getter
@AllArgsConstructor
public enum ValidEnum {
    PARAMETER_ERROR(5001,"参数格式有误");

    private Integer code;
    private String message;
}

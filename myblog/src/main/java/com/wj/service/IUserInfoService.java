package com.wj.service;

import com.wj.pojo.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2022-05-29
 */
public interface IUserInfoService extends IService<UserInfo> {

}

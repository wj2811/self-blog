package com.wj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wj.dto.UserInfoDTO;
import com.wj.pojo.User;
import com.wj.vo.PagingVO;

import java.util.List;

/**
* @author 悟君
* @description 针对表【t_user】的数据库操作Mapper
* @createDate 2022-05-26 15:20:19
* @Entity com.wj.pojo.User
*/
public interface UserMapper extends BaseMapper<User> {


    List<UserInfoDTO> getPagingUsers(PagingVO pagingVo);

    Integer selectCountUser(PagingVO pagingVo);
}

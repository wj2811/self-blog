package com.wj.controller;

import com.wj.pojo.UserInfo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wj
 * @Date 2022/5/26 16:57
 * @Description: TODO
 * @Version 1.0
 */
@RestController
public class TestController {
    @GetMapping("/test1")
    public String test1(){
        SecurityContextHolder.getContext().getAuthentication().getAuthorities().forEach(System.out::println);
        return "需要权限的访问";
    }

    @GetMapping("/test2")
    public Object test2(HttpServletRequest request){
        return new UserInfo();
    }
}
